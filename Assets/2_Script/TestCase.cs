﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class TestCase : MonoBehaviour {

	public enum TestStat{None, PlayOnly, Record, Replay};

	public delegate object[] GenerateCallback(float time);
	public delegate void ApplyCallback(GameObject gameObject, object[] values);

	public delegate object[] CaptureCallback(GameObject gameObject);

	public delegate double GetErrorCallback(object[] generated, object[] captured);

	public delegate string ValueToString(object[] values);

	public TestCase.GenerateCallback generator=null;
	public TestCase.ApplyCallback applier=null;
	public TestCase.CaptureCallback capturer=null;
	public TestCase.GetErrorCallback errorGetter=null;

	public TestCase.ValueToString toString = null;

	public Text generatedValueTxt=null;
	public Text capturedValueTxt=null;
	public Text errorTxt=null;
	// Use this for initialization

	private TestStat stat;
	private DateTime startTime;

	private double errSum;
	private double err2Sum;
	private double maxErr;

	int nSample;

	void Start () {
		stat = TestStat.None;
	}

	public void startRecord(){
		stat = TestStat.Record;
		startTime = DateTime.Now;
		gameObject.SetActive (true);
	}

	public void startPlayOnly(){
		stat = TestStat.PlayOnly;
		startTime = DateTime.Now;
		gameObject.SetActive (true);
	}

	public void startReplay(){
		stat = TestStat.Replay;
		startTime = DateTime.Now;
		gameObject.SetActive (true);
		errSum = err2Sum = maxErr = 0.0;
		nSample = 0;
	}

	public void stop(){
		if (stat == TestStat.PlayOnly) {
		} else if (stat == TestStat.Record) {

		} else if (stat == TestStat.Replay) {
		}
		stat = TestStat.None;
		gameObject.SetActive (false);
	}
		
	// Update is called once per frame
	void Update () {
		DateTime now;
		if(stat == TestStat.PlayOnly || stat == TestStat.Record){
			if (applier != null && generator!=null) {
				now = DateTime.Now;
				applier (gameObject, generator ((float)(now.Ticks - startTime.Ticks) * 0.0000001f));
			}
		}
	}

	void LateUpdate(){
		DateTime now;
		if(stat == TestStat.Replay){
			if (errorGetter != null && generator != null && capturer != null) {
				now = DateTime.Now;
				double curerr = errorGetter (generator ((float)(now.Ticks - startTime.Ticks) * 0.0000001f),capturer(gameObject));
				errSum += curerr;
				err2Sum += curerr * curerr;
				if (curerr > maxErr)
					maxErr = curerr;
				nSample++;	
				if (toString != null) {
					if (generatedValueTxt != null) {
						generatedValueTxt.text = toString (generator ((float)(now.Ticks - startTime.Ticks) * 0.0000001f));
					}
					if (capturedValueTxt != null) {
						capturedValueTxt.text = toString (capturer(gameObject));
					}
					if (errorTxt != null) {
						errorTxt.text = curerr.ToString ();
					}
				}
			}
		}		
	}

	public bool getErrors(out double errAve, out double errMod, out double errMax){
		if (nSample <= 0) {
			errAve = -1.0;
			errMod = -1.0;
			errMax = -1.0;
			return false;
		}
		errAve = errSum / nSample;
		errMod = Math.Sqrt (err2Sum / nSample - errAve * errAve);
		errMax = maxErr;
		return true;
	}
}
